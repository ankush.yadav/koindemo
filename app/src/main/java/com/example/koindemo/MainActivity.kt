package com.example.koindemo

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.koindemo.model.CustomToast
import com.example.koindemo.model.DisplayableToast
import com.example.koindemo.model.ToastFunction
import com.example.koindemo.util.CommonUtil
import com.example.koindemo.util.CommonUtil.Constaint
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import org.koin.java.KoinJavaComponent.get


class MainActivity : AppCompatActivity() {

//  Lazy injected PopupMessage
    private val toastFunction by inject<DisplayableToast>(named(Constaint.CUSTOM_MESSAGE))

    private val mainPresenter: CustomToast by inject { parametersOf(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Run and test
        btn_generate_toast.setOnClickListener {
            when(btn_generate_toast.text.toString()){
                Constaint.DEFAULT_MESSAGE -> {
                    btn_generate_toast.text = Constaint.POPUP_MESSAGE
                    get<ToastFunction>().displayToast()
                }
                Constaint.POPUP_MESSAGE -> {
                    btn_generate_toast.text = Constaint.CUSTOM_MESSAGE
                    get<DisplayableToast>(named(Constaint.POPUP_MESSAGE)).displayToast()
                }
                Constaint.CUSTOM_MESSAGE -> {
                    btn_generate_toast.text = Constaint.DEFAULT_MESSAGE
                    get<CustomToast>{ parametersOf("Parameter ")}.displayCustomToast()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
    }
}
