package com.example.koindemo.model

import com.example.koindemo.util.CommonUtil
import com.example.koindemo.util.CommonUtil.*
import org.koin.core.qualifier.named
import org.koin.dsl.module


val toastModule = module {
//    Create instances at start
    factory { ToastMessage() }
    single { ToastFunction(get(), get()) }

//    distinguish two definitions with the same type(Interface)
    single<DisplayableToast> { ToastFunction(get(), get()) }
    single<DisplayableToast>(named(Constaint.POPUP_MESSAGE)) { PopupMessage(get(), get()) }

//    Declaring injection parameters
    single { (view: String) -> CustomToast(view, get()) }

//    Dealing with generics
    single(named("Int")) { ArrayList<Int>() }
    single(named("String")) { ArrayList<String>() }

}