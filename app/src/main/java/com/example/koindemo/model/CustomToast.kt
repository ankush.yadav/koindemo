package com.example.koindemo.model

import android.content.Context
import android.widget.Toast

class CustomToast(private val message: String, private val context: Context) {
    fun displayCustomToast() {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}
