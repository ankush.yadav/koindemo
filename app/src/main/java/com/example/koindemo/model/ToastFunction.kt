package com.example.koindemo.model

import android.content.Context
import android.util.Log
import android.widget.Toast

interface DisplayableToast{
    fun displayToast()
}

class ToastFunction(private val toastMessage: ToastMessage, context: Context): DisplayableToast {
    private val context = context
    override fun displayToast() {
        Log.d("ToastFunction ", this.toString())
        Log.d("ToastMessage ", toastMessage.toString())
        Toast.makeText(context, toastMessage.toastMessage, Toast.LENGTH_LONG).show()
    }
}