package com.example.koindemo.model

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class KoinDemo: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@KoinDemo)
            modules(toastModule)
        }
    }

}